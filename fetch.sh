#!/usr/bin/env sh
set -euo

CMDNAME=$(basename "$0")

while getopts f: OPT
do
    case $OPT in
        'f' )
            FLAG_FILE='t'
            FILEPATH="$OPTARG";;
        * )
            echo "Usage: $CMDNAME [-f FILENAME]" 1>&2
            exit 1 ;;
   esac
done

if [ -z "$FLAG_FILE" ]; then
    echo "FILEPATH is required." 1>&2
    exit 1
fi

USERNAME=$(
    awk '/username=/ {sub("username=", "", $0); print $1}' < "$FILEPATH")
if [ -z "$USERNAME" ]; then
    echo "username is unset." 1>&2
    exit 1
fi

PASSWORD=$(
    awk '/password=/ {sub("password=", "", $0); print $0}' < "$FILEPATH" |
    base64 -d -)
if [ -z "$PASSWORD" ]; then
    echo "password is unset." 1>&2
    exit 1
fi

WEBDAV_HOST=$(
    awk '/webdav_host=/ {sub("webdav_host=", "", $0); print $0}' < "$FILEPATH")
if [ -z "$WEBDAV_HOST" ]; then
    echo "webdav_host is unset." 1>&2
    exit 1
fi

WEBDAV_PATH=$(
    awk '/webdav_path=/ {sub("webdav_path=", "", $0); print $0}' < "$FILEPATH")
if [ -z "$WEBDAV_PATH" ]; then
    echo "webdav_path is unset." 1>&2
    exit 1
fi

ORG_FILES=$(curl -sSL -u "$USERNAME:$PASSWORD" --url "$WEBDAV_HOST$WEBDAV_PATH" -X PROPFIND \
    | parsrx \
    | awk -v path="$WEBDAV_PATH" '/^\/d:multistatus\/d:response\/d:href[[:space:]](.+)\.org$/ {sub(path, "", $0); print $2 }')

for i in $ORG_FILES;
do
    { curl -sSL -X GET -u "$USERNAME:$PASSWORD" "$WEBDAV_HOST$WEBDAV_PATH$i" > "$i.dist"
      if [ "$(diff -q "$i" "$i.dist" | wc -l)" -eq 0 ]; then
          rm "$i.dist"
      fi }&
done
