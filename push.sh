#!/usr/bin/env sh

CMDNAME=$(basename "$0")

while getopts f: OPT
do
    case $OPT in
        'f' )
            FLAG_FILE='t'
            FILEPATH="$OPTARG";;
        * )
            echo "Usage: $CMDNAME [-f FILENAME]" 2>&1
            exit 1 ;;
   esac
done

if [ -z $FLAG_FILE ]; then
    echo "FILEPATH is required." 2>&1
    exit 1
fi

USERNAME=$(
    awk '/username=/ {sub("username=", "", $0); print $1}' < "$FILEPATH")
if [ -z "$USERNAME" ]; then
    echo "username is unset." 2>&1
    exit 1
fi

PASSWORD=$(
    awk '/password=/ {sub("password=", "", $0); print $0}' < "$FILEPATH" | \
    base64 -d -)
if [ -z "$PASSWORD" ]; then
    echo "password is unset." 2>&1
    exit 1
fi

WEBDAV_HOST=$(
    awk '/webdav_host=/ {sub("webdav_host=", "", $0); print $0}' < "$FILEPATH")
if [ -z "$WEBDAV_HOST" ]; then
    echo "webdav_host is unset." 2>&1
    exit 1
fi

WEBDAV_PATH=$(awk '/webdav_path=/ {sub("webdav_path=", "", $0); print $0}' < "$FILEPATH")
if [ -z "$WEBDAV_PATH" ]; then
    echo "webdav_path is unset." 2>&1
    exit 1
fi

IFS='
'
for dist in ./*.org.dist
do
	{ org=$(echo "$dist" | sed -r 's/(.+)\.dist$/\1/')
	  echo "$org"
	  curl -sSL -T "$org" -X PUT -u "$USERNAME:$PASSWORD" "$WEBDAV_HOST$WEBDAV_PATH$org"
    } &
done
