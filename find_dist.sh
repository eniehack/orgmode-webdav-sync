#!/usr/bin/env sh

for org in $(ls *.org);
do
    for dist in $(ls *.org.dist | sed -r 's/\.org\.dist/\.org')
    do
        if [ "$org" != "$dist" ]; then
            echo "$org"
        fi
    done
done
